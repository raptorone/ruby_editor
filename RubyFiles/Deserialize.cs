﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RubyFiles
{
    public class Deserialize
    {
        public static LevelObjectS1 LevelFile(Stream stream)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            LevelObjectS1 levelObject = (LevelObjectS1)formatter.Deserialize( stream );
            stream.Close();
            return levelObject;
        }

    }
}
