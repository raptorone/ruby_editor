﻿using System;
using System.Collections.Generic;

namespace RubyFiles
{
    [System.Serializable]
    public class LevelObjectS1
    {
        public int width;
        public int height;
        public Position3S1 playerPosition;
        public int[,] roomArray;
        public int[,] zoneArray;
        public int numberOfShifts;
        public List<ItemObjectS1> objects;

        public LevelObjectS1( int width, int height )
        {
            this.width = width;
            this.height = height;
            this.playerPosition = new Position3S1( 0, 0, 0 );
            this.roomArray = new int[width, height];
            this.zoneArray = new int[width, height];

            this.objects = new List<ItemObjectS1>();
        }
    }
    [System.Serializable]
    public class ItemObjectS1
    {
        public string name;
        public Position3S1 position;
        public Dictionary<string, string> parameters;

        public ItemObjectS1( string name, Position3S1 position )
        {
            this.name = name;
            this.position = position;
            this.parameters = new Dictionary<string, string>();
        }

        public ItemObjectS1( string name, Position3S1 position, Dictionary<string, string> parameters )
        {
            this.name = name;
            this.position = position;
            this.parameters = parameters;
        }
    }
    [System.Serializable]
    public class Position3S1
    {
        public float x;
        public float y;
        public float z;

        public Position3S1( float x, float y, float z )
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
}
