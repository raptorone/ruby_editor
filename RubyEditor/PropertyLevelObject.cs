﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RubyEditor
{
    public class PropertyLevelObject
    {
        [Description("The coordinates where the player will spawn.")]
        public PropertyPosition3 PlayerPosition { get; set; }
        [Description( "An array representing the location of floor tiles." )]
        public int[,] RoomArray { get; set; }
        [Description( "An array representing the location of zone tiles and thier state." )]
        public int[,] ZoneArray { get; set; }
        [Description( "How many times the player will be able to switch zones." )]
        public int NumberOfShifts { get; set; }
        [Description( "A list containing all game objects." )]
        public List<PropertyItemObject> Objects { get; set; }
        [ReadOnly( true )]
        [Description( "The width of the level (Cannot be changed)." )]
        public int Width { get; set; }
        [ReadOnly( true )]
        [Description( "The height of the level (Cannot be changed)." )]
        public int Height { get; set; }

        public PropertyLevelObject(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.RoomArray = new int[width, height];
            this.ZoneArray = new int[width, height];
            this.PlayerPosition = new PropertyPosition3( 0, 0, 0 );
            this.Objects = new List<PropertyItemObject>();
        }

        
    }
    [TypeConverter( typeof( ExpandableObjectConverter ) )]
    public class PropertyItemObject
    {
        [Description( "Indicates what kind of object it is." )]
        public string Name { get; set; }
        [Description( "The location where the object will spawn." )]
        public PropertyPosition3 Position { get; set; }
        [Editor(typeof(NotifyingCollectionEditor), typeof(UITypeEditor))]
        [Description( "A list of all parameters for this object." )]
        public List<PropertyStringPair> Parameters { get; set; }

        public PropertyItemObject()
        {
            this.Name = "<name>";
            this.Position = new PropertyPosition3(0,0,0);
            this.Parameters = new List<PropertyStringPair>();
        }

        public PropertyItemObject( string name, PropertyPosition3 position )
        {
            this.Name = name;
            this.Position = position;
            this.Parameters = new List<PropertyStringPair>();
        }

        public PropertyItemObject( string name, PropertyPosition3 position, List<PropertyStringPair> parameters )
        {
            this.Name = name;
            this.Position = position;
            this.Parameters = parameters;
        }
    }

    [TypeConverter( typeof( ExpandableObjectConverter ) )]
    public class PropertyPosition3
    {
        [Description( "The position along the x axis." )]
        public float X { get; set; }
        [Description( "The position along the y axis." )]
        public float Y { get; set; }
        [Description( "The position along the z axis (Recommended value: 0)." )]
        public float Z { get; set; }

        public PropertyPosition3( float x, float y, float z )
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
    }

    public class PropertyStringPair
    {
        [Description( "The name for the parameter." )]
        public string Key { get; set; }
        [Description( "The value of the parameter." )]
        [Editor(typeof(PropertyPairValueEditor), typeof(UITypeEditor))]
        public string Value { get; set; }

        public PropertyStringPair()
        {
            this.Key = "<key>";
            this.Value = "<value>";
        }

        public PropertyStringPair( string key, string value )
        {
            this.Key = key;
            this.Value = value;
        }

    }
}
