﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RubyEditor
{
    public partial class ResizeDialog : Form
    {

        public int parsedWidth;
        public int parsedHeight;
        public ResizeDialog(int currentWidth, int currentHeight)
        {
            InitializeComponent();
            label2.Text = "Width: " + currentWidth;
            label3.Text = "Height: " + currentHeight;
        }

        private void button1_Click( object sender, EventArgs e )
        {
            try
            {
                parsedWidth = int.Parse( textBox1.Text );
                parsedHeight = int.Parse( textBox2.Text );
                if( parsedWidth > 0 && parsedHeight > 0 )
                {
                    Close();
                    DialogResult = DialogResult.OK;
                    return;
                }
            }
            catch( Exception ex )
            {
                Console.Write( ex.StackTrace );
            }

            MessageBox.Show( "Width and Height must be integers between 1 and 1000", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning );
        }

        private void button2_Click( object sender, EventArgs e )
        {
            Close();
            DialogResult = DialogResult.Cancel;
        }
    }
}
