﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RubyFiles;

namespace RubyEditor
{
    class LevelObjectConverter
    {

        public static PropertyLevelObject ConvertToPropertyObject( LevelObjectS1 levelObjectS1 )
        {

            PropertyLevelObject propertyLevelObject = new PropertyLevelObject( levelObjectS1.width, levelObjectS1.height );
            propertyLevelObject.NumberOfShifts = levelObjectS1.numberOfShifts;
            propertyLevelObject.PlayerPosition = new PropertyPosition3( levelObjectS1.playerPosition.x, levelObjectS1.playerPosition.y, levelObjectS1.playerPosition.z);
            propertyLevelObject.RoomArray = levelObjectS1.roomArray;
            propertyLevelObject.ZoneArray = levelObjectS1.zoneArray;

            foreach( ItemObjectS1 itemObjectS1 in levelObjectS1.objects )
            {

                propertyLevelObject.Objects.Add(ConvertToPropertyObject(itemObjectS1) );
            }

            return propertyLevelObject;
        }

        public static PropertyItemObject ConvertToPropertyObject( ItemObjectS1 itemObjectS1 )
        {

            List<PropertyStringPair> propertyStringPairList = new List<PropertyStringPair>();

            foreach( string key in itemObjectS1.parameters.Keys )
            {
                propertyStringPairList.Add( new PropertyStringPair( key, itemObjectS1.parameters[key] ) );
            }

            PropertyItemObject propertyItemObject = new PropertyItemObject( itemObjectS1.name,
                new PropertyPosition3( itemObjectS1.position.x, itemObjectS1.position.y, itemObjectS1.position.z ),
                propertyStringPairList );

            return propertyItemObject;
        }


        public static LevelObjectS1 ConvertToObjectS1( PropertyLevelObject propertyLevelObject )
        {

            LevelObjectS1 levelObjectS1 = new LevelObjectS1( propertyLevelObject.Width, propertyLevelObject.Height );
            levelObjectS1.numberOfShifts = propertyLevelObject.NumberOfShifts;
            levelObjectS1.playerPosition = new Position3S1( propertyLevelObject.PlayerPosition.X, propertyLevelObject.PlayerPosition.Y, propertyLevelObject.PlayerPosition.Z );
            levelObjectS1.roomArray = propertyLevelObject.RoomArray;
            levelObjectS1.zoneArray = propertyLevelObject.ZoneArray;

            foreach( PropertyItemObject propertyItemObject in propertyLevelObject.Objects )
            {

                levelObjectS1.objects.Add( ConvertToObjectS1( propertyItemObject ) );
            }

            return levelObjectS1;
        }

        public static ItemObjectS1 ConvertToObjectS1( PropertyItemObject propertyItemObject )
        {
            Dictionary<string, string> parameterDictionary = new Dictionary<string, string>();

            foreach( PropertyStringPair propertyStringPair in propertyItemObject.Parameters )
            {
                parameterDictionary.Add( propertyStringPair.Key, propertyStringPair.Value );
            }

            ItemObjectS1 itemObjectS1 = new ItemObjectS1(propertyItemObject.Name, 
                new Position3S1( propertyItemObject.Position.X, propertyItemObject.Position.Y, propertyItemObject.Position.Z),
                parameterDictionary);

            return itemObjectS1;
        }

        public static LevelObjectS1 CloneObjectS1( LevelObjectS1 objectToClone )
        {

            LevelObjectS1 levelObjectS1 = new LevelObjectS1( objectToClone.width, objectToClone.height );
            levelObjectS1.numberOfShifts = objectToClone.numberOfShifts;
            levelObjectS1.playerPosition = new Position3S1( objectToClone.playerPosition.x, objectToClone.playerPosition.y, objectToClone.playerPosition.z );
            levelObjectS1.roomArray = (int[,]) objectToClone.roomArray.Clone();
            levelObjectS1.zoneArray = (int[,]) objectToClone.zoneArray.Clone();

            foreach( ItemObjectS1 itemObject in objectToClone.objects )
            {

                levelObjectS1.objects.Add( CloneObjectS1( itemObject ) );
            }

            return levelObjectS1;
        }

        public static ItemObjectS1 CloneObjectS1( ItemObjectS1 objectToClone )
        {
            Dictionary<string, string> parameterDictionary = new Dictionary<string, string>();

            foreach( string key in objectToClone.parameters.Keys )
            {
                parameterDictionary.Add( key, objectToClone.parameters[key] );
            }

            ItemObjectS1 itemObjectS1 = new ItemObjectS1( objectToClone.name,
                new Position3S1( objectToClone.position.x, objectToClone.position.y, objectToClone.position.z ),
                parameterDictionary );

            return itemObjectS1;
        }
    }
}
