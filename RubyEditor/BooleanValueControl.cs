﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace RubyEditor
{
    public partial class BooleanValueControl : UserControl
    {
        private IWindowsFormsEditorService editorService;
        private bool isControlReadyForInput;
        public string Result { get; private set; }

        public BooleanValueControl( IWindowsFormsEditorService editorService )
        {
            InitializeComponent();
            this.editorService = editorService;
        }

        public void SetValue( string value )
        {
            Result = value;
            bool result;
            if( bool.TryParse( value, out result ) )
            {
                radioButton1.Checked = result;
                radioButton2.Checked = !result;
            }
            else
            {
                radioButton1.Checked = false;
                radioButton2.Checked = false;
            }
            isControlReadyForInput = true;
        }

        private void radioButton1_CheckedChanged( object sender, EventArgs e )
        {
            Result = "True";
            if( isControlReadyForInput )
                editorService.CloseDropDown();
        }

        private void radioButton2_CheckedChanged( object sender, EventArgs e )
        {
            Result = "False";
            if(isControlReadyForInput)
                editorService.CloseDropDown();
        }
    }
}
