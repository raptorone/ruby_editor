﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RubyEditor
{
    public partial class PropertyEditor : Form
    {
        public object propertyLevelObject;
        public PropertyEditor(object displayObject)
        {
            InitializeComponent();
            propertyLevelObject = displayObject;
            propertyGrid1.SelectedObject = propertyLevelObject;
        }

    }
}
