﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace RubyEditor
{
    public partial class HexColorValueControl : UserControl
    {
        private static Dictionary<string, Color> colorDictionary = new Dictionary<string, Color>{
            {"Red", Color.FromArgb(230, 25, 25) },
            {"Green", Color.FromArgb(60, 180, 75) },
            {"Yellow", Color.FromArgb(255, 255, 25) },
            {"Blue", Color.FromArgb(0, 130, 200) },
            {"Orange", Color.FromArgb(245, 130, 48) },
            {"Purple", Color.FromArgb(145, 30, 180) },
            {"Cyan", Color.FromArgb(70, 240, 240) },
            {"Magenta", Color.FromArgb(240, 50, 230) },
            {"Lime", Color.FromArgb(210, 245, 60) },
            {"Pink", Color.FromArgb(250, 190, 190) },
            {"Teal", Color.FromArgb(0, 128, 128) },
            {"Lavender", Color.FromArgb(230, 190, 255) },
            {"Brown", Color.FromArgb(170, 110, 40) },
            {"Maroon", Color.FromArgb(128, 0, 0) },
            {"Mint", Color.FromArgb(170, 255, 195) },
            {"Olive", Color.FromArgb(128, 128, 0) },
            {"Coral", Color.FromArgb(255, 215, 180) },
            {"Navy", Color.FromArgb(0, 0, 128) },
            {"Grey", Color.FromArgb(128, 128, 128) },
            {"Forest", Color.FromArgb(0, 128, 0) },

        };
        private static ImageList imageList;

        public string Result { get; set; }

        private IWindowsFormsEditorService editorService;

        static HexColorValueControl()
        {
            imageList = new ImageList();
            foreach( Color color in colorDictionary.Values )
            {
                imageList.Images.Add( CreateBitmapOfColor(color) );
            }
        }

        public HexColorValueControl( IWindowsFormsEditorService editorService )
        {
            InitializeComponent();
            listView1.SmallImageList = imageList;

            int index = 0;
            foreach( string s in colorDictionary.Keys )
            {
                listView1.Items.Add( new ListViewItem(s, index));
                index++;
            }
            this.editorService = editorService;
        }

        public void SetColor( string color )
        {
            // if( colorDictionary.ContainsValue( color ) )
            //  {
            //      string name = colorDictionary.First( x => x.Value == color ).;
            //      listView1.Items.IndexOf()
            //  }
            Result = color;
        }

        private static Bitmap CreateBitmapOfColor( Color color )
        {
            Bitmap bitmap = new Bitmap( 16, 16 );
            using( Graphics graphics = Graphics.FromImage( bitmap ) )
                graphics.Clear( color );
            return bitmap;
        }

        private void button1_Click( object sender, EventArgs e )
        {
            Result = ColorTranslator.ToHtml( colorDictionary[listView1.SelectedItems[0].Text] );
            editorService.CloseDropDown();
        }
    }
}
