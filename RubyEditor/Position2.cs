﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RubyEditor
{
    public class Position2
    {
        public float x;
        public float y;
    
        public Position2( float x, float y )
        {
            this.x = x;
            this.y = y;
        }
    }
}
