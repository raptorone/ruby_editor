﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RubyEditor
{
    class _2DArray
    {

        public static void Resize<T>( ref T[,] array, int newWidth, int newHeight )
        {
            int width = Math.Min(array.GetLength( 0 ), newWidth);
            int height = Math.Min(array.GetLength( 1 ), newHeight);

            T[,] newArray = new T[newWidth, newHeight];

            for( int y = 0; y < height; y++ )
            {
                for( int x = 0; x < width; x++ )
                {
                    newArray[x, y] = array[x, y];
                }
            }

            array = newArray;
        }

    }
}
