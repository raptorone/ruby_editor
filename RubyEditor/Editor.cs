﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using RubyFiles;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace RubyEditor
{
    class Editor
    {
        private static Dictionary<string, Image> objectImageDict = new Dictionary<string, Image> {
            {"Orb", Properties.Resources.orb },
            {"Telepad", Properties.Resources.telepad },
            {"Button", Properties.Resources.button},
            {"Door", Properties.Resources.door },
            {"Winpad", Properties.Resources.winpad },
            {"Box", Properties.Resources.box },
        };
        private static Dictionary<string, Dictionary<string, string>> defaultParameterDict = new Dictionary<string, Dictionary<string, string>> {
            {"Orb", new Dictionary<string, string>{ { "color", "<hexcolor>" } } },
            {"Telepad", new Dictionary<string, string>{ { "color", "<hexcolor>" } } },
            {"Button", new Dictionary<string, string>{ { "color", "<hexcolor>" }, { "isNotted", "False" } } },
            {"Door", new Dictionary<string, string>{ { "color", "<hexcolor>" }, { "totalSignalsRequired", "<integer>" }, { "rotated", "False" } } },
        };
        private static Dictionary<string, string> emptyParameter = new Dictionary<string, string> { { "<key>", "<value>" } };

        private static Image missingImage = Properties.Resources.missingtexture;

        private LevelObjectS1 levelObject;

        private Panel panel;
        private EditorControls controls;

        private List<LevelObjectS1> levelObjectTimeline = new List<LevelObjectS1>();
        private int currentTimelineIndex;

        private float xMargin = 0;
        private float yMargin = 0;
        private float cellSize;

        private Graphics graphicsObj;

        public event PropertyChangedEventHandler IsSavedChanged;
        private bool _isSaved = true;
        public bool IsSaved
        {
            get {
                return _isSaved;
            }
            private set {
                _isSaved = value;
                IsSavedChanged(this, new PropertyChangedEventArgs("IsSaved"));
            }
        }
    
        private int lineWidth = 2;
        private Pen pen = new Pen( Color.DimGray, 2 );
        private Pen highlightPen = new Pen( Color.Cyan, 2 );
        private SolidBrush whiteBrush = new SolidBrush( Color.White );
        private SolidBrush redBrush = new SolidBrush( Color.Red );
        private SolidBrush greenBrush = new SolidBrush( Color.LimeGreen );
        private SolidBrush blackBrush = new SolidBrush( Color.Black );

        private SolidBrush textureMissingBrush = new SolidBrush( Color.Magenta );

        private Position2 highlightedPos = new Position2(0,0);
        private ItemObjectS1 selectedItemObject;
        private PropertyItemObject selectedPropertyItemObject;


        public Editor( LevelObjectS1 levelObject, ref Panel panel, ref EditorControls controls )
        {
            this.levelObject = levelObject;

            this.panel = panel;
            this.controls = controls;

            levelObjectTimeline.Add( LevelObjectConverter.CloneObjectS1( this.levelObject ) );
            currentTimelineIndex = 0;
            
            controls.LevelLoaded();
            ResizedDraw();
        }

        public void ResizedDraw()
        {
            ResizeCalculations();
            Draw();
        }

        public void ResizingDraw()
        {
            ResizeCalculations();
            DrawGrid();
        }

        private void ResizeCalculations()
        {
            float panelAspectRatio = (float)panel.Width / panel.Height;
            float gridAspectRatio = (float)levelObject.width / levelObject.height;


            if( panelAspectRatio > gridAspectRatio )
            {
                cellSize = (float)(panel.Height - lineWidth) / levelObject.height;
                xMargin = (panel.Width - (cellSize * levelObject.width)) / 2;
                yMargin = 0;
            }
            else
            {
                cellSize = (float)(panel.Width - lineWidth) / levelObject.width;
                yMargin = (panel.Height - (cellSize * levelObject.height)) / 2;
                xMargin = 0;
            }


            graphicsObj = panel.CreateGraphics();

            graphicsObj.Clear( Color.White );
        }


        public void Draw()
        {
            DrawGrid();

            for( int x = 0; x < levelObject.width; x++ )
            {
                for( int y = 0; y < levelObject.height; y++ )
                {
                    DrawCellOnly( x, y );
                }
            }

            foreach( ItemObjectS1 itemObject in levelObject.objects)
            {
                DrawItemObject( itemObject );
            }
        }

        private void DrawGrid()
        {
            for( int i = 0; i < levelObject.width + 1; i++ )
            {
                float offset = 1 + xMargin + cellSize * i;
                graphicsObj.DrawLine( pen, offset, yMargin, offset, panel.Height - yMargin );
            }

            for( int i = 0; i < levelObject.height + 1; i++ )
            {
                float offset = 1 + yMargin + cellSize * i;
                graphicsObj.DrawLine( pen, xMargin, offset, panel.Width - xMargin, offset );
            }
        }

        public void HighlightCell( float x, float y )
        {
            x -= xMargin;
            y -= yMargin;

            float gridX = x / (cellSize);
            float gridY = y / (cellSize);

            if( gridX >= 0 && gridX < levelObject.width && gridY >= 0 && gridY < levelObject.height )
            {
                if( controls.ActiveLayer == Layer.OBJECTS )
                {
                    SetObjectGridPosition( ref gridX, ref gridY );
                }
                else
                {
                    gridX = (int)gridX;
                    gridY = (int)gridY;
                }

                if( !(gridX == highlightedPos.x && gridY == highlightedPos.y) )
                {
                    DrawOuterRect( pen, highlightedPos.x, highlightedPos.y );

                    if( controls.ActiveLayer == Layer.OBJECTS )
                    {
                        UpdateCell( (int)Math.Floor(highlightedPos.x), (int)Math.Floor( highlightedPos.y ));
                        UpdateCell( (int)Math.Ceiling(highlightedPos.x), (int)Math.Floor( highlightedPos.y ));
                        UpdateCell( (int)Math.Floor(highlightedPos.x), (int)Math.Ceiling( highlightedPos.y ));
                        UpdateCell( (int)Math.Ceiling(highlightedPos.x), (int)Math.Ceiling( highlightedPos.y ));
                    }

                    DrawOuterRect( highlightPen, gridX, gridY );
                    highlightedPos = new Position2( gridX, gridY );
                }
     
  
                    controls.SetCoordStatusText("(" + x + "," + y + ") (" + highlightedPos.x + "," + highlightedPos.y + ")");
            }
        }

        public void MarkCell( float x, float y, int value )
        {

            x -= xMargin;
            y -= yMargin;

            float gridX = x / cellSize;
            float gridY = y / cellSize;

            if( gridX >= 0 && gridX < levelObject.width && gridY >= 0 && gridY < levelObject.height )
            {
                if( controls.ActiveLayer == Layer.FLOOR )
                {
                    int floorValue = Math.Min( value, 1 );
                    if( levelObject.roomArray[(int)gridX, (int)gridY] != floorValue )
                    {
                        levelObject.roomArray[(int)gridX, (int)gridY] = floorValue;
                        LevelObjectChanged();
                    }
                }
                else if( controls.ActiveLayer == Layer.ZONES )
                {
                    if( levelObject.zoneArray[(int)gridX, (int)gridY] != value )
                    {
                        levelObject.zoneArray[(int)gridX, (int)gridY] = value;
                        LevelObjectChanged();
                    }
                }
                else if( controls.ActiveLayer == Layer.OBJECTS )
                {
                    SetObjectGridPosition( ref gridX, ref gridY );

                    if( value == 1 )
                    {
                        if(controls.ObjectName != null && controls.ObjectName != "")
                        {

                            bool positionEmpty = true;
                            foreach( ItemObjectS1 itemObject in levelObject.objects )
                            {
                                if( itemObject.position.x == gridX
                                    && itemObject.position.y == gridY)
                                {
                                    positionEmpty = false;
                                    break;
                                }
                            }
                            if( positionEmpty )
                            {
                                Dictionary<string, string> parameters;
                                if( defaultParameterDict.ContainsKey( controls.ObjectName ) )
                                {
                                    parameters = defaultParameterDict[controls.ObjectName];
                                }
                                else
                                {
                                    parameters = emptyParameter;
                                }

                                ItemObjectS1 itemObject = new ItemObjectS1( controls.ObjectName, new Position3S1( gridX, gridY, 0 ), parameters );
                                levelObject.objects.Add( itemObject );
                                selectedItemObject = itemObject;
                                selectedPropertyItemObject = LevelObjectConverter.ConvertToPropertyObject( itemObject );
                                controls.SetPropertyGridSelectedObject( selectedPropertyItemObject );
                                LevelObjectChanged();
                            }
                        }
                        else {
                            MessageBox.Show( "Object must have name", "Invalid Input", MessageBoxButtons.OK, MessageBoxIcon.Warning );
                        }
                    }
                    else if( value == 2 )
                    {
                        ItemObjectS1 itemObject = GetItemObjectNear( gridX, gridY );
                        if( itemObject != null )
                        {
                            selectedItemObject = itemObject;
                            selectedPropertyItemObject = LevelObjectConverter.ConvertToPropertyObject( itemObject );
                            controls.SetPropertyGridSelectedObject( selectedPropertyItemObject );
                        }
                    }
                    else if( value == 0 )
                    {
                        ItemObjectS1 itemObject = GetItemObjectNear( gridX, gridY );
                        if( itemObject != null )
                        {
                            levelObject.objects.Remove( itemObject );
                            resetObjectControls();
                            LevelObjectChanged();
                        }
                    }
                }

                UpdateCell( (int)gridX, (int)gridY );
            }

        }

        private void DrawCellOnly( int x, int y )
        {
            if( levelObject.zoneArray[x, y] == 1 && controls.ZonesVisible )
            {
                DrawFilledRect( greenBrush, x, y );
            }
            else if( levelObject.zoneArray[x, y] == 2 && controls.ZonesVisible )
            {
                DrawFilledRect( redBrush, x, y );
            }
            else if( levelObject.roomArray[x, y] == 1 && controls.FloorVisible )
            {
                DrawFilledRect( whiteBrush, x, y );
            }
            else
            {
                DrawFilledRect( blackBrush, x, y );
            }
        }

        private void UpdateCell( int x, int y )
        {
            if( x >= 0 && x < levelObject.width && y >= 0 && y < levelObject.height )
            {

                DrawCellOnly(x, y);

                if( controls.ObjectsVisible )
                {
                    foreach( ItemObjectS1 itemObject in levelObject.objects )
                    {
                        if( itemObject.position.x < x + 1 && itemObject.position.x > x - 1
                            && itemObject.position.y < y + 1 && itemObject.position.y > y - 1 )
                        {
                            DrawItemObject(itemObject);
                        }
                    }
                }
            }
        }

        private void DrawItemObject( ItemObjectS1 itemObject )
        {
            Image objectImage = objectImageDict.ContainsKey( itemObject.name ) ? objectImageDict[itemObject.name] : missingImage;
            Color color = Color.PaleGoldenrod;
            if( itemObject.parameters.ContainsKey( "color" ) )
            {
                try
                {
                    color = ColorTranslator.FromHtml( itemObject.parameters["color"] );
                }
                catch( Exception e )
                {
                    Console.Write( e.StackTrace );
                }
            }
            DrawImageRect( objectImage, color, itemObject.position.x, itemObject.position.y );
        }

        

        private ItemObjectS1 GetItemObjectNear( float gridX, float gridY )
        {
            
            foreach( ItemObjectS1 itemObject in levelObject.objects )
            {
                if( itemObject.position.x < gridX + 0.25 && itemObject.position.x > gridX - 0.25
                    && itemObject.position.y < gridY + 0.25 && itemObject.position.y > gridY - 0.25 )
                {
                    return itemObject;
                }
            }
            return null;
        }

        private void SetObjectGridPosition( ref float gridX, ref float gridY)
        {
            if( controls.ObjectsSnapToGrid )
            {
                gridX = (float)Math.Floor( Math.Max( Math.Min( gridX, levelObject.width - 0.5 ), 0.5 ) * 2 - 0.5 ) / 2;
                gridY = (float)Math.Floor( Math.Max( Math.Min( gridY, levelObject.height - 0.5 ), 0.5 ) * 2 - 0.5 ) / 2;
            }
            else
            {
                gridX = (float)Math.Max( Math.Min( gridX, levelObject.width - 0.5 ), 0.5 ) - 0.5f;
                gridY = (float)Math.Max( Math.Min( gridY, levelObject.height - 0.5 ), 0.5 ) - 0.5f;
            }
        }

        private void LevelObjectChanged()
        {
            IsSaved = false;
            if( currentTimelineIndex < levelObjectTimeline.Count - 1 )
            {
                controls.SetRedoEnabled( false );
                levelObjectTimeline.RemoveRange( currentTimelineIndex + 1, levelObjectTimeline.Count - 1 - currentTimelineIndex );
            }
            levelObjectTimeline.Add( LevelObjectConverter.CloneObjectS1( levelObject ) );
            if( levelObjectTimeline.Count > 100 )
            {
                levelObjectTimeline.RemoveAt( 0 );
            }
            else
            {
                currentTimelineIndex++;
                controls.SetUndoEnabled( true );
            }
        }

        public void SaveToFile( string filePath )
        {
            Stream stream = File.Open( filePath, FileMode.Create );
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize( stream, levelObject );
            stream.Close();

            IsSaved = true;
        }

        public void ObjectPropertyChanged()
        {
            levelObject.objects.Remove( selectedItemObject );
            selectedItemObject = LevelObjectConverter.ConvertToObjectS1( selectedPropertyItemObject );
            levelObject.objects.Add( selectedItemObject );
            Draw();

            LevelObjectChanged();
        }

        public void OpenPropertyDialog()
        {
            PropertyEditor propertyEditor = new PropertyEditor( LevelObjectConverter.ConvertToPropertyObject( levelObject ) );
            propertyEditor.ShowDialog();
            levelObject = LevelObjectConverter.ConvertToObjectS1( (PropertyLevelObject)propertyEditor.propertyLevelObject );
            resetObjectControls();
            ResizedDraw();

            LevelObjectChanged();
        }

        public void OpenChangeSizeDialog()
        {
            ResizeDialog resizeDialog = new ResizeDialog(levelObject.width, levelObject.height);
            if( resizeDialog.ShowDialog() == DialogResult.OK )
            {
                _2DArray.Resize<int>( ref levelObject.roomArray, resizeDialog.parsedWidth, resizeDialog.parsedHeight );
                _2DArray.Resize<int>( ref levelObject.zoneArray, resizeDialog.parsedWidth, resizeDialog.parsedHeight );
                levelObject.width = resizeDialog.parsedWidth;
                levelObject.height = resizeDialog.parsedHeight;
                ResizedDraw();
            }
        }

        public void Undo()
        {
            currentTimelineIndex--;
            setLevelObjectFromTimeline();
        }

        public void Redo()
        {
            currentTimelineIndex++;
            setLevelObjectFromTimeline();
        }

        private void setLevelObjectFromTimeline()
        {
            IsSaved = false;

            if( currentTimelineIndex < levelObjectTimeline.Count - 1 )
                controls.SetRedoEnabled( true );
            else
                controls.SetRedoEnabled( false );
            if( currentTimelineIndex < 1 )
                controls.SetUndoEnabled( false );
            else
                controls.SetUndoEnabled( true );

            levelObject = LevelObjectConverter.CloneObjectS1( levelObjectTimeline[currentTimelineIndex] );
            resetObjectControls();
            ResizedDraw();
        }


        private void resetObjectControls()
        {
            selectedPropertyItemObject = null;
            selectedItemObject = null;
            controls.SetPropertyGridSelectedObject( null );
        }

        private void DrawFilledRect( Brush brush, int x, int y )
        {
            graphicsObj.FillRectangle( brush, 2 + xMargin + cellSize * x, 2 + yMargin + cellSize * y, cellSize - 2, cellSize - 2 );
        }

        private void DrawFilledRect( Brush brush, float x, float y )
        {
            graphicsObj.FillRectangle( brush, 2 + xMargin + cellSize * x, 2 + yMargin + cellSize * y, cellSize - 2, cellSize - 2 );
        }

        private void DrawImageRect( Image image, Color color, float x, float y )
        {
   
            ImageAttributes ia = new ImageAttributes();
            ColorMap colorMap = new ColorMap();
            colorMap.OldColor = Color.Black;
            colorMap.NewColor = color;
            ColorMap[] remapTable = { colorMap };

            ia.SetRemapTable( remapTable, ColorAdjustType.Bitmap );
            graphicsObj.DrawImage(image, new Rectangle(new Point( (int)(2 + xMargin + cellSize * x), (int)(2 + yMargin + cellSize * y) ), new Size((int)cellSize, (int)cellSize) ), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, ia);
        }

        private void DrawOuterRect( Pen pen, int x, int y )
        {
            graphicsObj.DrawRectangle( pen, 1 + xMargin + cellSize * x, 1 + yMargin + cellSize * y, cellSize, cellSize );
        }

        private void DrawOuterRect( Pen pen, float x, float y )
        {
            graphicsObj.DrawRectangle( pen, 1 + xMargin + cellSize * x, 1 + yMargin + cellSize * y, cellSize, cellSize );
        }
    }

    public enum Layer
    {
        FLOOR, ZONES, OBJECTS, SELECT
    }
}
