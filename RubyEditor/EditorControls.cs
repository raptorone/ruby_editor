﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RubyEditor
{
    class EditorControls
    {
        public bool FloorVisible { get; set; }
        public bool ZonesVisible { get; set; }
        public bool ObjectsVisible { get; set; }
        public Layer ActiveLayer { get; set; }
        public string ObjectName { get; set; }
        public bool ObjectsSnapToGrid { get; set; }

        private ToolStripMenuItem editPropsMenuItem;
        private ToolStripMenuItem undoMenuItem;
        private ToolStripMenuItem redoMenuItem;
        private ToolStripMenuItem changeSizeMenuItem;
        private ToolStripMenuItem saveMenuItem;
        private ToolStripMenuItem saveAsMenuItem;
        private ToolStripMenuItem reDrawMenuItem;

        private ToolStripStatusLabel coordsLabel;

        private CheckBox floorVisibilityCheckBox;
        private CheckBox zonesVisibilityCheckBox;
        private CheckBox objectVisibilityCheckBox;

        private CheckBox objectsSnapToGridCheckBox;

        private RadioButton editFloorRadioButton;
        private RadioButton editZonesRadioButton;
        private RadioButton editObjectsRadioButton;

        private ComboBox objectNameComboBox;
        private PropertyGrid objectPropertyGrid;

        public EditorControls( ref ToolStripMenuItem undoMenuItem, ref ToolStripMenuItem redoMenuItem, ref ToolStripMenuItem editPropsMenuItem, ref ToolStripMenuItem changeSizeMenuItem, ref ToolStripMenuItem saveMenuItem, ref ToolStripMenuItem saveAsMenuItem, ref ToolStripMenuItem reDrawMenuItem, ref ToolStripStatusLabel coordsLabel, ref CheckBox objectsSnapToGridCheckBox, ref CheckBox floorVisibilityCheckBox, ref CheckBox zonesVisibilityCheckBox, ref CheckBox objectVisibilityCheckBox, ref RadioButton editFloorRadioButton, ref RadioButton editZonesRadioButton, ref RadioButton editObjectsRadioButton, ref ComboBox objectNameComboBox, ref PropertyGrid objectPropertyGrid )
        {
            this.editPropsMenuItem = editPropsMenuItem;
            this.undoMenuItem = undoMenuItem;
            this.redoMenuItem = redoMenuItem;
            this.changeSizeMenuItem = changeSizeMenuItem;
            this.saveMenuItem = saveMenuItem;
            this.saveAsMenuItem = saveAsMenuItem;
            this.reDrawMenuItem = reDrawMenuItem;
            this.coordsLabel = coordsLabel;  

            this.floorVisibilityCheckBox = floorVisibilityCheckBox;
            this.zonesVisibilityCheckBox = zonesVisibilityCheckBox;
            this.objectVisibilityCheckBox = objectVisibilityCheckBox;

            this.objectsSnapToGridCheckBox = objectsSnapToGridCheckBox;

            this.editFloorRadioButton = editFloorRadioButton;
            this.editZonesRadioButton = editZonesRadioButton;
            this.editObjectsRadioButton = editObjectsRadioButton;

            this.objectNameComboBox = objectNameComboBox;
            this.objectPropertyGrid = objectPropertyGrid;

            FloorVisible = this.floorVisibilityCheckBox.Checked;
            ZonesVisible = this.zonesVisibilityCheckBox.Checked;
            ObjectsVisible = this.objectVisibilityCheckBox.Checked;

            ObjectsSnapToGrid = this.objectsSnapToGridCheckBox.Checked;

            if( editFloorRadioButton.Checked )
            {
                ActiveLayer = Layer.FLOOR;
            }
            else if( editZonesRadioButton.Checked )
            {
                ActiveLayer = Layer.ZONES;
            }
            else if( editObjectsRadioButton.Checked )
            {
                ActiveLayer = Layer.OBJECTS;
            }
        }

        public void SetPropertyGridSelectedObject( object obj )
        {
            objectPropertyGrid.SelectedObject = obj;
            objectPropertyGrid.ExpandAllGridItems();
        }

        public void LevelLoaded()
        {
            SetPropertyGridSelectedObject( null );
            editPropsMenuItem.Enabled = true;
            changeSizeMenuItem.Enabled = true;
            saveMenuItem.Enabled = true;
            saveAsMenuItem.Enabled = true;
            reDrawMenuItem.Enabled = true;
        }

        public void SetCoordStatusText( string s )
        {
            coordsLabel.Text = s;
        }

        public void SetUndoEnabled( bool value )
        {
            undoMenuItem.Enabled = value;
        }

        public void SetRedoEnabled( bool value )
        {
            redoMenuItem.Enabled = value;
        }
    }
}
