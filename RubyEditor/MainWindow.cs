﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RubyFiles;

namespace RubyEditor
{
    public partial class MainWindow : Form
    {
        EditorControls editorControls;
        Editor editor;

        string saveFilePath = null;
        bool isResizing = false;

        public MainWindow()
        {            
            InitializeComponent();
            NotifyingCollectionEditor.PropertyValueChanged += new RubyEditor.NotifyingCollectionEditor.PropertyValueChangedEventHandler( propertyGrid1_PropertyValueChanged );
            editorControls = new EditorControls(ref undoToolStripMenuItem, ref redoToolStripMenuItem, ref editPropertiesToolStripMenuItem, ref changeSizeToolStripMenuItem, ref saveToolStripMenuItem, ref saveAsToolStripMenuItem, ref reDrawToolStripMenuItem, ref toolStripCoordsLabel, ref checkBox4, ref checkBox2, ref checkBox3, ref checkBox1, ref radioButton1, ref radioButton2, ref radioButton3, ref comboBox1, ref propertyGrid1 );
            if( Program.initialFilePath != null )
            {
                OpenFile( Program.initialFilePath );
            }
        }

        private void Editor_IsSavedChanged( object sender, PropertyChangedEventArgs e )
        {
            this.Text = saveFilePath == null ? "Ruby Level Editor" : "Ruby Level Editor - " + saveFilePath;

            if( !editor.IsSaved )
            {
                this.Text += "*";
            }
        }

        private void panel2_Paint( object sender, PaintEventArgs e )
        {
            if( !isResizing )
            {
                Draw();
            }
        }

        private void MainWindow_ResizeBegin( object sender, EventArgs e )
        {
            isResizing = true;
        }

        private void MainWindow_Resize( object sender, EventArgs e )
        {
            ResizingDraw();
        }

        private void MainWindow_ResizeEnd( object sender, EventArgs e )
        {
            ResizedDraw();
            isResizing = false;
        }

        private void checkBox2_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.FloorVisible = checkBox2.Checked;
            Draw();
        }

        private void checkBox3_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.ZonesVisible = checkBox3.Checked;
            Draw();
        }

        private void checkBox1_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.ObjectsVisible = checkBox1.Checked;
            Draw();
        }

        private void checkBox4_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.ObjectsSnapToGrid = checkBox4.Checked;
        }

        private void radioButton1_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.ActiveLayer = Layer.FLOOR;
        }

        private void radioButton2_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.ActiveLayer = Layer.ZONES;
        }

        private void radioButton3_CheckedChanged( object sender, EventArgs e )
        {
            editorControls.ActiveLayer = Layer.OBJECTS;
        }

        private void panel2_MouseMove( object sender, MouseEventArgs e )
        {
            editor.HighlightCell( e.X, e.Y );

            MarkCell( e );
        }

        private void panel2_MouseDown( object sender, MouseEventArgs e )
        {
            MarkCell( e );
        }

        private void MarkCell( MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
            {
                if( Control.ModifierKeys == Keys.Control )
                {
                    editor.MarkCell( e.X, e.Y, 2 );
                }
                else
                {
                    editor.MarkCell( e.X, e.Y, 1 );
                }
            }
            else if( e.Button == MouseButtons.Right )
            {
                editor.MarkCell( e.X, e.Y, 0 );
            }
        }

        private void comboBox1_TextChanged( object sender, EventArgs e )
        {
            editorControls.ObjectName = comboBox1.Text;
        }

        private void saveToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if( saveFilePath == null )
            {
                if( saveFileDialog1.ShowDialog() == DialogResult.OK )
                {
                    saveFilePath = saveFileDialog1.FileName;
                }
            }
            editor.SaveToFile( saveFilePath );
        }

        private void saveAsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if( saveFileDialog1.ShowDialog() == DialogResult.OK )
            {
                saveFilePath = saveFileDialog1.FileName;
                editor.SaveToFile( saveFilePath );
            }
        }

        private void openToolStripMenuItem_Click( object sender, EventArgs e )
        {

            if( openFileDialog1.ShowDialog() == DialogResult.OK )
            {
                OpenFile( openFileDialog1.FileName );
            }
        }

        private void aboutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            AboutBox1 aboutBox = new AboutBox1();
            aboutBox.ShowDialog();
        }

        private void newToolStripMenuItem_Click( object sender, EventArgs e )
        {
            CreateDialog createDialog = new CreateDialog();
            if( createDialog.ShowDialog() == DialogResult.OK )
            {
                editor = new Editor( new LevelObjectS1( createDialog.parsedWidth, createDialog.parsedHeight ), ref panel2, ref editorControls );
                saveFilePath = null;
                Editor_IsSavedChanged( null, null );
                editor.IsSavedChanged += Editor_IsSavedChanged;
                panel2.Visible = true;
            }
        }

        private void reDrawToolStripMenuItem_Click( object sender, EventArgs e )
        {
            ResizedDraw();
        }

        private void editPropertiesToolStripMenuItem_Click( object sender, EventArgs e )
        {
            editor.OpenPropertyDialog();
        }

        private void propertyGrid1_PropertyValueChanged( object s, PropertyValueChangedEventArgs e )
        {
            editor.ObjectPropertyChanged();
        }

        private void MainWindow_FormClosing( object sender, FormClosingEventArgs e )
        {
            if( editor != null && !editor.IsSaved )
            {
                if( MessageBox.Show( "You have unsaved work, are you sure you want to exit?", "Are You Sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question ) == DialogResult.No )
                {
                    e.Cancel = true;
                }
            }
        }


        private void OpenFile( string filePath )
        {
            Stream stream = File.Open( filePath, FileMode.Open );
            editor = new Editor( Deserialize.LevelFile( stream ), ref panel2, ref editorControls );
            saveFilePath = filePath;
            Editor_IsSavedChanged( null, null );
            editor.IsSavedChanged += Editor_IsSavedChanged;
            panel2.Visible = true;
        }

        private void Draw()
        {
            if( editor != null )
                editor.Draw();
        }
        private void ResizedDraw()
        {
            if( editor != null )
                editor.ResizedDraw();
        }
        private void ResizingDraw()
        {
            if( editor != null )
                editor.ResizingDraw();
        }

        private void changeSizeToolStripMenuItem_Click( object sender, EventArgs e )
        {
            editor.OpenChangeSizeDialog();
        }

        private void undoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            editor.Undo();
        }

        private void redoToolStripMenuItem_Click( object sender, EventArgs e )
        {
            editor.Redo();
        }
    }
}
